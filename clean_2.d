#!/usr/bin/env rdmd
/* SPDX-License-Identifier: MIT */
// This grabs all the data that comes after the data from the 1 min file
// The overlapping data doesn't exactly match (???) but whatever
// The output of this needs to be reversed and concatenated onto the end of the output of the last script
void main(string[] args)
{
    auto file = File("raw_data/Bitstamp_BTCUSD_d.csv");
    foreach (record; csvReader!(string[string])(file.byLine.joiner("\n"), null))
    {
        auto timestamp = record["unix"];
        auto unix_time = timestamp.to!long;
        if (unix_time <= 1522108800L) continue;
        auto systime = SysTime.fromUnixTime(unix_time, UTC());
        if (systime.second) continue;
        if (systime.minute) continue;
        if (systime.hour) continue;
        auto price = record["open"];
        "%s,%s".writefln(timestamp, price);
    }
}
import std;
