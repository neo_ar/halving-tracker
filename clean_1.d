#!/usr/bin/env rdmd
/* SPDX-License-Identifier: MIT */
//This converts 2012-2018 bitstamp 1 min data into daily data and extracts the fields we care about
void main(string[] args)
{
    auto file = File("raw_data/bitstampUSD_1-min_data_2012-01-01_to_2018-03-27.csv");
    "Timestamp,Price".writeln;
    foreach (record; csvReader!(string[string])(file.byLine.joiner("\n"), null))
    {
        auto timestamp = record["Timestamp"];
        auto systime = SysTime.fromUnixTime(timestamp.to!long, UTC());
        if (systime.second) continue;
        if (systime.minute) continue;
        if (systime.hour) continue;
        auto price = record["Open"];
        "%s,%s".writefln(timestamp, price);
    }
}
import std;
