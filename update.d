#!/usr/bin/env dub
/+ dub.sdl:
    name "update"
    dependency "scriptlike" version="~>0.10.3"
+/
/* SPDX-License-Identifier: MIT */
// This grabs new data from bitstamp
// The output of this needs to be concatenated onto the end of cleaned.csv
// WARNING: Assumes no errors
void main(string[] args)
{
    auto file = File("cleaned.csv");
    auto timestamp = [`tail -n 1 cleaned.csv | cut -d "," -f 1`].sh;
    auto last_time = SysTime.fromUnixTime(timestamp.to!long, UTC());
    auto curr_time = Clock.currTime(UTC());
    auto limit = (curr_time - last_time).total!"days";
    auto json = [
        "curl -s",
        mixin(interp!`"https://www.bitstamp.net/api/v2/ohlc/btcusd/?step=86400&limit=${limit}"`)
    ].sh.parseJSON["data"]["ohlc"].array;
    foreach (record; json)
        mixin(interp!`${record["timestamp"].str.to!long},${record["open"].str.to!double}`).writeln;
}
string sh(string[] cmd){ return cmd.join(" ").runCollect.strip; }
import scriptlike; import std;
