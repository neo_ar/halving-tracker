#!/usr/bin/python
# SPDX-License-Identifier: MIT
# This is a reverse-engineering of @bitstein's @HalvingTracker
import csv # Historical price data has been prepared as a csv file
from datetime import datetime, timezone # Deal with unix timestamps
import math # We need to scale using logarithms
import matplotlib.pyplot as plt # For plotting the data
from matplotlib.ticker import MultipleLocator, StrMethodFormatter # and making it pretty
dates = []
prices = []
halvings = [
        # I just googled the dates of the halvings for this
        datetime(2012, 11, 28, 0, 0, tzinfo=timezone.utc),
        datetime(2016, 7, 9, 0, 0, tzinfo=timezone.utc),
        datetime(2020, 5, 11, 0, 0, tzinfo=timezone.utc),
        datetime(2024, 4, 19, 0, 0, tzinfo=timezone.utc)
]
# cleaned.csv is our historical price data
with open('cleaned.csv') as f:
    csv_reader = csv.DictReader(f)
    # Read the dates into `dates` as datetime objects
    # and read the prices into `prices` as floats
    for row in csv_reader:
        dates.append(datetime.fromtimestamp(int(row['Timestamp']), timezone.utc))
        prices.append(float(row['Price']))
# Extract the 2012 halving from the data
head = dates.index(halvings[0])
tail = dates.index(halvings[1]) - 1
h0 = prices[head:tail]
# Extract the 2016 halving from the data
head = dates.index(halvings[1])
tail = dates.index(halvings[2]) - 1
h1 = prices[head:tail]
# Extract the 2020 halving from the data
head = dates.index(halvings[2])
tail = dates.index(halvings[3]) - 1
h2 = prices[head:tail]
# Extract the 2024 halving from the data
head = dates.index(halvings[3])
h3 = prices[head:]
# Compute the logs of the prices within each halving
l0 = [math.log10(p) for p in h0]
l1 = [math.log10(p) for p in h1]
l2 = [math.log10(p) for p in h2]
# We use the first price from the 2024 halving for scaling
ref = math.log10(h3[0])
# Scale the 2012, 2016, and 2020 halvings to the 2024 halving
# `p + (ref - first data point in log-halving)` so they all start at the same point
# and then `10 ** that` to undo the log space we were working in
s0 = [10 ** (p + (ref - l0[0])) for p in l0]
s1 = [10 ** (p + (ref - l1[0])) for p in l1]
s2 = [10 ** (p + (ref - l2[0])) for p in l2]
# Now that our data is prepared, we can plot it
fig, ax = plt.subplots() # Need ax for formatting the axis and fig for spacing
plt.plot(s0, label='2012') # Plot scaled 2012 halving
plt.plot(s1, label='2016') # Plot scaled 2016 halving
plt.plot(s2, label='2020') # Plot 2020 halving
plt.plot(h3, label='2024') # Plot 2024 halving
# I'm not going to bother making the line colours match
plt.title('Bitcoin Price After Halvings', fontweight='bold') # Plot title
plt.yscale('log') # Plot prices in log scale
# Original halving tracker takes out the first set of 0-10,000 minor axis
# I'm not going to bother with that
ax.xaxis.set_major_locator(MultipleLocator(50)) # Here we set x-axis to have a tick every 50 days
ax.set_xlim(0, 1450) # ... from 0 to 1450
ax.set_ylim(top=10000000) # and here we set the y-axis from 0 to $10 Mill
ax.yaxis.set_minor_formatter(StrMethodFormatter('${x:,.0f}')) # Label prices on minor ticks
ax.yaxis.set_major_formatter(StrMethodFormatter('${x:,.0f}')) # Label prices on major ticks
plt.xlabel('Days After Halving') # x-axis label
plt.ylabel('Price (Scaled to 2024 Halving)') # y-axis label
plt.grid(which='minor', linestyle=':') # minor grid lines
plt.grid(which='major', linestyle='--') # major grid lines
ax.legend() # Automatic legend
# I'm not going to bother placing the legend in top-right
# @bitstein's plot didn't distinguish between major and minor ticks
# I went with dotted lines for minor and dashed for major here
# Furthermore, he has his ticks formatted on all sides
# and all the same length due to no major/minor difference
# I'm not going to bother with any of that
# Space the figure out with values from subplot-configuration
fig.subplots_adjust(top=0.961, bottom=0.065, left=0.078, right=0.98, hspace=0.2, wspace=0.2)
# Make it look the same as on my screen
# LVDS-2 connected primary 1600x900+0+0 (normal left inverted right x axis y axis) 344mm x 194mm
# 344 mm = 13.5433 in, 194 mm = 7.6378 in
fig.set_size_inches(13.5433, 7.6378)
#   resolution:    96x96 dots per inch
plt.savefig('halving_tracker.png', dpi=96, bbox_inches='tight')
