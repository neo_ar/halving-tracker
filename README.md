# Reverse Engineered Halving Tracker

## Data sources
* I found a 2012-2018 1-min CSV from bitstamp on the internets
* I also found 2014-Present Daily CSV from bitstamp on the internets

## Preparing the data
* The opening prices from each day were taken and a single cleaned.csv was produced
    * Wrote 2 little D scripts (clean_1.d and clean_2.d) to do that

## Plotting the data
* A python script (plot.py) was written to ingest cleaned.csv and output a png image

## Updating the data
* I wrote one more little D script (update.d) to pull in new data into cleaned.csv

## End Result
![Halving Tracker](halving_tracker.png "Halving Tracker")

## Exercise for the reader
* This does not currently reproduce the twitter bot

